package com.imemories.automation.getpageobjects;

public enum Locators {
	id, name, classname, css, xpath, linktext;
}