package com.imemories.keywords;

import org.openqa.selenium.WebDriver;

import com.imemories.automation.getpageobjects.GetPage;

public class LoginPage extends GetPage {

	public LoginPage(WebDriver driver) {
		super(driver, "Login");
	}

	public String getTitle() {
		return getPageTitle();
	}

	public void enterUsername(String username) {
		element("inp_loginEmail").sendKeys(username);
	}

	public void enterPassword(String password) {
		element("inp_loginPassword").sendKeys(password);
	}
	
	public void clickSubmit() {
		element("btn_submit").click();
	}
	
	public void clickSignUp() {
		element("btn_click").click();
	}

	public boolean verifyUserIsOnLoginPage() {
		return isElementDisplayed("remember_me_checkbox");
	}
	
}
