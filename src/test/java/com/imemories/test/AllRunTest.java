package com.imemories.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.imemories.StepDefs.RunFeatureFile;

@RunWith(Suite.class)
@Suite.SuiteClasses({ RunFeatureFile.class})
public class AllRunTest {

}
