Page Title: Quest

#Object Definitions
===================================================================================================================
vessel_icon_on_map			css										div.gmnoprint>img
vessel_info_box				css										div.gm-style-iw div div>div
label_username				css										.nav-user-name
header_text					css										.ship-icon~h1
inp_searchBox				id										searchBox
list_geo_searchResult		css										ul.search-result-list>li:nth-child(1)
label_location				css										.gm-style>div div>div.gmnoprint:first-child
tab_zones					css										[role='tablist']>li:nth-child(2)>a
lnk_create_zone				css										.create-zone>a
box_zone_type				css										.map-controllers
btns_zone					css										.map-controllers>.action-area>button
box_save_zone				css										#save h5.modal-title
inp_zone_name				css										#save div.modal-footer>input
btn_save_zone				css										#save div.modal-footer>button
list_zones					css										.zone-list li>a>span:nth-child(2)
select_zone					css										.zone-list>li:not(:nth-child(n+6))
zone_box_optns				css										.action-area>button
btn_accumulation_alrt		xpath									.//button[text()='Set Accumulation Alerts']
edit_vessel_type			css										.accumulation-container>div:nth-child(1) .edit
select_vessel_type			css										.filter-vessel.custom-checkbox .row>div:nth-of-type(2) input
btn_save_changes			css										button[class='btn btn-primary ']
threshold_amount			css										input.accumulation-amount
notification_email_type		css										div.alert-row>div>div:nth-child(1)>label:nth-child(1)>input
btn_next					css										.accumulation-container>div:nth-child(5) button:first-of-type
btn_save_alert				css										#accumulation button:first-of-type
btn_back					css										#alert-save button:last-of-type
save_alert_msg				css										#alert-save p
header_txt					css										.dashboard-content h1
cross_icon					css										.qs-icon-search
zone_icn_tlbr				css										.qs-icon-zone
lnk_zoom_in					css										div[title='Zoom in']
lnk_zoom_out				css										div[title='Zoom out']
flter_vssl_type				css										#filter-container label:nth-child(1) span.checbox
flter_bulk_cer				css										#filter-container label:nth-child(3) span.checbox
vessel_viewport				css										div[class='collapse-panel open']
cancel_button				css										.action-area button:nth-child(2)
delete_zone					xpath									.//button[text()='Delete Zone']
cnfrm_delete				xpath									.//button[text()='Delete']
edit_zone					xpath									.//button[text()='Edit Zone']
save_changes				xpath									.//button[text()='Save Changes']
cancel_edit					xpath									.//button[text()='Cancel']
edit_text					css										.modal-footer input
save_edit					xpath									.//button[text()='Save']
set_accum_alerts			xpath									.//button[text()='Set Accumulation Alerts']
vessels_filter				css										.exposure-map div[style*='marker-circle']
side_panel					css										.collapse-panel
filter_tab					css										.collapse-tabs a	
side_panel_icon				css										.collapse-handle span
zone_toolbar				css										.map-headerbar .active
cross_icon_search			css										.search-in-panel button span.qs-icon-cross-white
close_icon					css										.close-editing span.qs-icon-cross	
map_scale					xpath									.//div[@class='exposure-map']/descendant::span[contains(text(),'100')]/following-sibling::div
inp_search_zone				css										#search-zone
delete_btn					xpath									.//button[text()='Delete Zone']
cancel_btn					xpath									.//button[text()='Cancel']
===================================================================================================================