Page Title: Quest

#Object Definitions
===================================================================================================================
inp_loginEmail						id										loginEmail
inp_loginPassword					id										loginPassword
btn_submit							css										button.btn-primary.btn-ripple
label_invalid_email					id										emailError
lnk_frgt_passwd						css										.btn-link
inp_resetEmail						id										resetEmail
header_reset						css										h2
reset_email_instruction_text		css										h2~p.text-info
user_avatar							css										.user-avatar
opt_logout							css										.dropdown-menu li:last-of-type a
opt_support							css										.dropdown-menu li:first-of-type a
===================================================================================================================