Page Title: Quest

#Object Definitions
===================================================================================================================
filter_zonename				id										search-zone
cross_icon					css										.qs-icon-cross-white
clear_cross_icon			css										.map-headerbar .btn-inside-right span.qs-icon-cross
zones_list					css										.zone-list li:not([style='display: none;'])
search_toolbar				css										.dashboard-content.panel-open .form-group input
search_result				css										.list-group.search-result-list li>span
===================================================================================================================