Page Title: Quest

#Object Definitions
===================================================================================================================
unsubscribe_alert			css					.search-hint>li:nth-child(1)
watchlist_header			css					.query-selection h1
unsubscribe_modal_bx		css					.modal-content p
modal_buttons				css					.modal-content button
unsubscribe_btn				css					.modal-content button:last-of-type
unsubscribe_list			css					.search-hint>li span.title
vessels_tab					xpath				.//div[@class='search-vessel watchlist']//descendant::a[text()='Vessels']
ports_tab					xpath				.//div[@class='search-vessel watchlist']//descendant::a[text()='Ports']
zones_tab					xpath				.//div[@class='search-vessel watchlist']//descendant::a[text()='Zones']
alert_settings				css					.search-hint>li:nth-child(1)
alert_header				css					#step1 .close-window~h1
v_alert_header				css					.accumulation-container>h3
vessels_list				css					.search-hint li
===================================================================================================================