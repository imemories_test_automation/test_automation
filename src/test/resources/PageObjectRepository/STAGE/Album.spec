Page Title: 

#Object Definitions
===================================================================================================================
inp_loginEmail						id										username
inp_loginPassword					xpath								    //*[@id="password"]
btn_submit							name									submitButton
album_image							classname								album-image
alb_click                           xpath                                   //*[@id="root"]/div/div/div[2]/header/div[2]/div[2]/div[1]
alb_modal                           css                                     .default-dialog
alb_title                           id                                      newAlbumName
alb_desc                            id                                      newAlbumDescription
alb_save                            id                                      submit-button
alb_title_list						classname								hover-editable
alb_menu_title						classname								album-thumbnail-menu-image
alb_delete_context_menu				css										.popup-menu>.popup-menu-item:nth-child(2)
alb_delete_btn                      css                                     .imem-button.primary.btn-dialog
alb_down_context_menu               css                                     .popup-menu>.popup-menu-item:nth-child(4)
alb_pop_menu                        classname                                popup-menu
alb_mng_dwld                        css                                     .imem-button.imem-button-link.primary
alb_dwnl_title                      css                                     .download-item div:nth-child(2)
alb_share_context_menu              css										.popup-menu>.popup-menu-item:nth-child(1)
copy_shrd_link                      css                                     .imem-button.secondary.imem-button-small
now_playing_title                   css                                     .js_now_playing_title
===================================================================================================================