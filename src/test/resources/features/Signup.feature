Feature: SignUp feature scenarios

Background: 
Given User is on login page
Then User clicks on Signup button

@smoke
Scenario: Verify new user is able to Signup with iMemories
When User enter 'test_first_name' as First Name
And User enter 'test_last_name' as Last Name
And User enter following Email address
| Email_Address |
| signupemailId |
And User enter '123456' as password
And User click on SignUp button
Then new user navigates to empty Albums page

@smoke
Scenario: Verify user is not able to Signup with existing email address
When User enter 'test_first_name' as First Name
And User enter 'test_last_name' as Last Name
And User enter 'matest6@032818.com' as Email address
And User enter '123456' as password
Then User click on SignUp button
Then User should not be able to create new account


